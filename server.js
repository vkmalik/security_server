'use strict';
require('dotenv').config;
const config = require('config');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const port = config.get('PORT');
const morgan = require('morgan');
const { db } = require('./app/models');

app.use(cors());

app.use(morgan('tiny'));
app.use(express.static('public'))
app.use('/', express.static('uploads'))


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


require('./app/routes')(app);

app.listen(port, () => {
    console.log(`App listen port @ ${port}`)
})
