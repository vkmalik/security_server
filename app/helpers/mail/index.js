'use strict';
const NewAccountVerification = require('./verify-new-user');
const SendPasswordResetMail = require('./send-forgot-password-verification-mail');
const sendContact = require('./contact-message-send');
module.exports = {
    NewAccountVerification,
    SendPasswordResetMail,
    sendContact
};