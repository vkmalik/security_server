'use strict';
const { SendMail } = require('../../lib');
const { URLSearchParams, URL } = require('url');
const config = require('config');
const HOST_ADDR = config.get('HOST_ADDR');

function getTemplate(otp, name) {
   const template = `
   <h2>Dear ${name}</h2>
   <p>This is an email from Araksha - Gaurd Patroling System.</p>
   <p>Please follow the <b>Authentication Code: </b> ${otp} to reset your password.
   </p>
   <p>Regards</p>
   <p>Araksha - Gaurd Patroling System</p>
`;

   return template;
}



async function send(otpToken, email, name) {
   const otp = otpToken;
   // const params = new URLSearchParams({
   //     otp
   // });

   // const host = HOST_ADDR.ui;
   // const url = new URL(`${host}/reset-password`);
   // url.search = params.toString();
   const html = getTemplate(otp, name);
   return SendMail.send(email, {
       html,
       subject: 'Password Reset Link'
   });
}




module.exports = {
   send
};