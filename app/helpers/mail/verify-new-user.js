'use strict';
const { SendMail } = require('../../lib');
const { URLSearchParams, URL } = require('url');
const config = require('config');
const HOST_ADDR = config.get('HOST_ADDR');


function getTemplate(url, name) {
    const template = `
    <h2>Dear ${name}</h2>
    <p>Welcome to your Smart Temp App.</p>
      <p> To complete your account activation and use your Smart Temp Comfort App you must click<a href="${url}">here</a>
      <p>We look forward to having you join our family of users,however should you have any concerns or questions please click <a href="mailto:support@smarttemp.com.au" target="_blank">here</a></p>
      <br/>
    </p>
    <p>Regards</p>
    <p>The Team at Smart Temp Australia P/L	<a href="www.smarttemp.com.au">www.smarttemp.com.au</a></p>
`;

    return template;
}




async function send(otpToken, email, name) {
    const otp = otpToken;
    const params = new URLSearchParams({
        otp,
        verify: 'account'
    });

    const host = HOST_ADDR.server;
    const url = new URL(`${host}/api/auth/signup`);
    url.search = params.toString();
    const html = getTemplate(url.href, name);
    return SendMail.send(email, {
        html,
        subject: 'Smart Temp: Activate your Account'
    });
}



module.exports = {
    send
};