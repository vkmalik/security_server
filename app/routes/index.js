'use strict';

const ProviderRouter = require('./provider');
const CompanyRouter = require('./company');
const EmployeeRouter = require('./employee');
const SiteRouter = require('./site');
const LocationRouter = require('./location');
const ScanRouter = require('./scan-detail');
const { ProviderLiginRouter } = require('./logins');
const forgotRouter = require('./forgot');


module.exports = app => {
    app.use('/', forgotRouter);
    app.use('/api/login', ProviderLiginRouter);
    app.use('/api/provider', ProviderRouter);
    app.use('/api/company', CompanyRouter);
    app.use('/api/employee', EmployeeRouter);
    app.use('/api/site', SiteRouter);
    app.use('/api/scan', ScanRouter);
    app.use('/api/location', LocationRouter);
}