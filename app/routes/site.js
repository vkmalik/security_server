'use strict';
const express = require('express');
const router = express.Router();
const { Site } = require('../models');


router.route('/')

  /*
   * Register New Site
   */

  .post(async (req, res, next) => {
    console.log('hello')
    const { body } = req;
    const {
      siteName,
      siteNoOfQrcodes,
    } = body;
    const query = {
      siteName
    };
    try {
      if (siteName === undefined || siteNoOfQrcodes === undefined) {
        const errorData = { message: 'All fields are mandatory' };
        return res.json({ errorData })
      }
      const site = await Site.findOne(query).exec();

      if (site) {
        const error = new Error('Site Already Exist...');
        error.status = 409;
        throw error;
      }

      const newSite = new Site(body);

      await newSite.save();
      res.json({ message: 'created sucessfully' });

    } catch (error) {
      next(error);
    }

  })

  /*
   * Get All Site
   */

  .get(async (req, res, next) => {
    try {
      const result = await Site.find({}).exec()
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/:id')

  /*
   * Get Particular Site find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { siteId: id };
    try {
      const result = await Site.findOne(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

  /*
   * Get Particular Site find And Update
   */

  .put(async (req, res, next) => {
    const { id } = req.params;
    const query = { siteId: id };
    try {
      const result = await Site.findOneAndUpdate(query, req.body, { new: true }).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

router.route('/company/:id')

  /*
   * Get Particular Site find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { companyId: id };
    try {
      const result = await Site.find(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })



router.route('/pages/:start/:end')
  /*
   * Get Particular Provider find
   */
  .get(async (req, res, next) => {
    const { start, end } = req.params;
    try {
      const result = await Site.find({}).skip(+start).limit(+end)
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/ids')
  .post(async (req, res, next) => {
    const { siteId } = req.body || {};
    try {
      const result = await Site.find({ siteId: { $in: siteId } }).exec()
      res.json(result)
    }
    catch (error) {
      next(error)
    }
  })

module.exports = router;
