'use strict';
const express = require('express');
const router = express.Router();
const { User, Employee } = require('../models');
const { PasswordServ } = require('../lib');
const path = require('path');
const uuid = require('uuid');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: './uploads/employee-pic',
  filename: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    const fileName = `${uuid()}-${Date.now()}${ext}`;
    cb(null, fileName);
  }
});
const upload = multer({ storage });
const cpUpload = upload.fields([{ name: 'profilePic', maxCount: 1 }, { name: 'governmentIdCardFrontImg', maxCount: 1 }, { name: 'governmentIdCardBackImg', maxCount: 1 }])



router.route('/')

  /*
   * Register New Employee
   */

  .post(cpUpload, async (req, res, next) => {
    const { body } = req;
    const {
      email,
      employeeName,
      role
    } = body;
    const companies = [];
    let com = JSON.parse(req.body.companies);

    delete req.body.companies;

    com.forEach(ele => {
      companies.push(ele);
    })

    Object.assign(body, { companies })

    if (req.files) {
      let getobjectKeys = Object.keys(req.files);
      getobjectKeys.forEach(function (ele, index) {
        body[ele] = `/employee-pic/${req.files[ele][0].filename}`
      });
    }
    const query = {
      email
    };
    try {
      if (email === undefined || employeeName === undefined || role === undefined) {
        const errorData = { message: 'All fields are mandatory' };
        return res.json({ errorData })
      }
      const user = await User.findOne(query).exec();

      if (user) {
        const error = new Error('User Already Exist...');
        error.status = 409;
        throw error;
      }

      const newUser = new User({
        email,
        role
      });

      const result = await newUser.save();
      const userId = result.id;
      const password = await PasswordServ.hash(body.password);
      const profileData = {
        userId,
        password,
      };

      Object.assign(body, profileData)
      const profile = new Employee(body);
      const results = await profile.save();
      // res.json({ message: 'created sucessfully' });
      res.json(results);

    } catch (error) {
      next(error);
    }

  })

  /*
   * Get All Employees
   */

  .get(async (req, res, next) => {
    try {
      const result = await Employee.find({}).exec()
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/:id')

  /*
   * Get Particular Employee find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { employeeId: id };
    try {
      const result = await Employee.findOne(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

  /*
   * Get Particular Employee find And Update
   */

  .put(cpUpload, async (req, res, next) => {
    const { body } = req;
    const { id } = req.params;
    const query = { employeeId: id };
    if (req.files) {
      let getobjectKeys = Object.keys(req.files)
      getobjectKeys.forEach(function (ele, index) {
        body[ele] = `/employee-pic/${req.files[ele][0].filename}`
      });
    }
    const companies = [];
    let com = JSON.parse(req.body.companies);

    delete req.body.companies;

    com.forEach(ele => {
      companies.push(ele);
    })

    Object.assign(body, { companies })
    try {
      const result = await Employee.findOneAndUpdate(query, body, { new: true }).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

router.route('/company/:id/:siteId')
  /*
  * Get Particular company Site find
  */

  .get(async (req, res, next) => {
    const { id } = req.params;
    try {
      const employeeList = [];
      const result = await Employee.find().exec()
      result.forEach(ele => {
        ele.companies.forEach(data => {
          if (data.companyId === id) {
            employeeList.push(ele)
          }
        })
      })
      if (employeeList.length === 0) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(employeeList)
    } catch (error) {
      next(error)
    }
  })

router.route('/company/:id')

  /*
   * Get Particular Site find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    // const query = { providerId: id };
    try {
      // const result = await Employee.find(query).exec()
      const employeeList = [];
      const result = await Employee.find().exec()
      result.forEach(ele => {
        if (ele.providerId === id) {
          employeeList.push(ele)
        } else {
          ele.companies.forEach(data => {
            if (data.companyId === id) {
              employeeList.push(ele)
            }
          })
        }
      })
      if (employeeList.length === 0) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(employeeList)
    } catch (error) {
      next(error)
    }
  })


router.route('/pages/:start/:end')
  /*
   * Get Particular Provider find
   */
  .get(async (req, res, next) => {
    const { start, end } = req.params;
    try {
      const result = await Employee.find({}).skip(+start).limit(+end)
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

module.exports = router;
