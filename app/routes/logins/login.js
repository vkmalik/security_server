'use strict';

const express = require('express');
const router = express.Router();
const { User, Provider, Company, Employee } = require('../../models');
const { PasswordServ, TokenServ } = require('../../lib');


async function profileCheck(id, role) {
    if (role === 'owner') {
        return await Provider.findOne({ userId: id }).exec();
    }
    if (role === 'super_admin') {
        return await Provider.findOne({ userId: id }).exec();
    }
    if (role === 'Admin') {
        return await Provider.findOne({ userId: id }).exec();
    }
    return await Employee.findOneAndUpdate({ userId: id }, { isLogin: true }, { new: true }).exec();
}



router.route('/')


/**
 * Provider Login
 * 
 */

.post(async(req, res, next) => {
    const {
        email,
        password
    } = req.body;

    try {
        const user = await User.findOne({ email }).exec();
        if (user) {
            if (user.isLogin === false) {
                await User.findOneAndUpdate({ email }, { isLogin: true }).exec();
            } else {
                const error = new Error('User Already Logged-In.')
                error.status = 403;
                const message = 'User Already Logged-In.'
                const errorData = { success: false, payload: message }
                res.json(errorData);
                return next(error);
            }
        }
        if (!user) {
            const error = new Error('Given Email is Not Exist')
            error.status = 400;
            const message = 'Given Email is Not Exist'
            const errorData = { success: false, payload: message }
            res.json(errorData);
            return next(error);
        }
        const profile = await profileCheck(user.id, user.role);

        const isCorrectPassword = await PasswordServ.match(password, profile.password);
        if (!isCorrectPassword) {
            const error = new Error('Incorrect Password')
            error.status = 400;
            return next(error);
        }

        const tokenData = {
            email,
            userId: user.id,
            role: user.role,
            providerId: profile.providerId || '',
            // companyId: profile.companyId || '',
            employeeId: profile.employeeId || '',
            name: profile.providerName || profile.companyName || profile.employeeName,
            // siteId: profile.siteId || ''
            companies: profile.companies
        };

        const token = await TokenServ.generate(tokenData);
        res.json({ token });

    } catch (error) {
        next(error);
    }

})

.get(async(req, res, next) => {
    try {
        const result = await Employee.find({ isLogin: true }).exec();
        res.json(result)
    } catch (error) {
        next(error)
    }
})

router.route('/users')
    .get(async(req, res, next) => {
        try {
            const result = await User.find({ role: 'Employee' }).exec();
            res.json(result)
        } catch (error) {
            next(error)
        }
    })

router.route('/loggedIn/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        try {
            const result = await User.findOne({ id: id }).exec();
            res.json(result)
        } catch (error) {
            next(error)
        }
    })

router.route('/checkemail/:id')
    .get(async(req, res, next) => {
        const { email } = JSON.parse(req.params.id);
        try {
            const result = await User.find({ email }).exec();
            res.json(result)
        } catch (error) {
            next(error)
        }
    })

router.route('/changePassword/:userId')
    .put(async(req, res, next) => {
        const { userId } = req.params || {};
        const {
            oldPassword,
            newpassword
        } = req.body;

        try {
            const profile = await Employee.findOne({ employeeId: userId }).exec();
            const isCorrectPassword = await PasswordServ.match(oldPassword, profile.password);
            if (!isCorrectPassword) {
                const error = new Error('Incorrect Password');
                error.status = 401;
                // const message = 'Incorrect Password'
                // const finalData = { success: false, payload: message }
                // res.json({ finalData });
                return next(error);
            }
            const hashPassword = await PasswordServ.hash(newpassword);
            await Object.assign(profile, { password: hashPassword }).save();
            const message = 'Password Updated Successfully'
            const finalData = { success: true, payload: message }
            res.json({
                finalData
            });


        } catch (error) {
            next(error);
        }

    })

router.route('/update')
    .put(async(req, res, next) => {
        const {
            email,
            isLogin
        } = req.body;
        try {
            const result = await User.findOneAndUpdate({ email }, { isLogin: isLogin }).exec();
            res.json(result)
        } catch (error) {
            next(error)
        }
    })


router.route('/logout')

.post(async(req, res, next) => {

    const {
        email,
    } = req.body;

    try {
        const user = await User.findOne({ email }).exec();
        if (user) {
            if (user.isLogin === true) {
                const result1 = await User.findOneAndUpdate({ email }, { isLogin: false }).exec();
                const message = 'Success'
                const errorData = { success: true, payload: message }
                res.json(errorData);
            } else {
                const error = new Error('User Already Logged-Out.')
                error.status = 403;
                const message = 'User Already Logged-Out.'
                const errorData = { success: false, payload: message }
                res.json(errorData);
                return next(error);
            }
        }
        const { id } = user || {};

        if (!user) {
            const error = new Error('Given Email is Not Exist')
            error.status = 400;
            return next(error);
        }
        const result = await Employee.findOneAndUpdate({ userId: id }, { isLogin: false }, { new: true }).exec();

        res.json({ result });

    } catch (error) {
        next(error);
    }

})


router.route('/forgot')
    .post(async(req, res, next) => {

        const {
            email
        } = req.body;

        try {
            const user = await User.findOne({ email }).exec();

            if (!user) {
                //const error = new Error('User Not Exist');
                //error.status = 400;
                //return next(error);
                const error = { message: 'User Not Exist' };
                const errorData = { success: false, payload: error }
                return res.json(errorData);
                //error.status = 400;
            }

            const otp = Math.floor(Math.random() * 10000)

            const profile = await LocalProfile.findOne({ userId: user.id }).exec();
            await Object.assign(profile, { otp }).save();
            SendPasswordResetMail.send(await otp, email, profile.firstName)
                .then(data => {})
                .catch(error => console.error(error));
            const message = 'OTP has been sent to your Email. Please use the OTP to reset your password'
            const finalData = { success: true, payload: message }
            res.json({ finalData });


        } catch (error) {
            next(error);
        }

    })


module.exports = router;