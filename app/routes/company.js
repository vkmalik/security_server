'use strict';
const express = require('express');
const router = express.Router();
const path = require('path');
const uuid = require('uuid');
const { User, Company } = require('../models');
const { PasswordServ } = require('../lib');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: './uploads/company-log',
  filename: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    const fileName = `${uuid()}-${Date.now()}${ext}`;
    cb(null, fileName);
  }
});
const upload = multer({ storage });



router.route('/')

  /*
   * Register New Provider
   */

  .post(upload.single('companyLogo'), async (req, res, next) => {
    const { body } = req;
    body.role = 'Admin'
    const {
      email,
      companyName,
      role
    } = body;
    const query = {
      email
    };
     if(req.file){
      body.profilePic = `/company-log/${req.file.filename}`;
    }
    try {
      if (email === undefined || companyName === undefined || role === undefined) {
        const errorData = { message: 'All fields are mandatory' };
        return res.json({ errorData })
      }
      const user = await User.findOne(query).exec();

      if (user) {
        const error = new Error('User Already Exist...');
        error.status = 409;
        throw error;
      }

      const newUser = new User({
        email,
        role
      });

      const result = await newUser.save();
      const userId = result.id;
      const password = await PasswordServ.hash(body.password);
      const profileData = {
        userId,
        password,
      };

      Object.assign(body, profileData)
      const profile = new Company(body);
      await profile.save();
      res.json({ message: 'created sucessfully' });

    } catch (error) {
      next(error);
    }

  })

  /*
   * Get All Provider
   */

  .get(async (req, res, next) => {
    try {
      const result = await Company.find({}).exec()
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/:id')

  /*
   * Get Particular Provider find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { companyId: id };
    try {
      const result = await Company.findOne(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

  /*
   * Get Particular Provider find And Update
   */

  .put(upload.single('companyLogo'), async (req, res, next) => {
    const { body } = req;
    const { id } = req.params;
    const query = { companyId: id };
     if(req.file){
      body.profilePic = `/company-log/${req.file.filename}`;
    }
    try {
      const result = await Company.findOneAndUpdate(query, body, { new: true }).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })
  

router.route('/pages/:start/:end')
  /*
   * Get Particular Provider find
   */
  .get(async (req, res, next) => {
    const { start, end } = req.params;
    try {
      const result = await Company.find({}).skip(+start).limit(+end)
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

  router.route('/provider/:id')

  /*
   * Get Particular Provider find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { providerId: id };
    try {
      const result = await Company.find(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })






module.exports = router;
