'use strict';
const express = require('express');
const router = express.Router();
const { Scan } = require('../models');


function getDateFormate(data) {
    const { fromDate, lastDate } = data || {};
    if (!fromDate || !lastDate) {
        return;
    }
    let splitFromData = fromDate.split('-')
    let splitEndData = lastDate.split('-')
    const fData = `${splitFromData[2]}/${splitFromData[1]}/${splitFromData[0]}`
    const lData = `${splitEndData[2]}/${splitEndData[1]}/${splitEndData[0]}}`
    return { fData, lData }

}





router.route('/')

/*
 * Register New Scan
 */

.post(async(req, res, next) => {
    const { body } = req;
    const getDate = new Date();
    var day = getDate.getDate();
    var month = getDate.getMonth() + 1;
    const year = getDate.getFullYear();
    var hour = getDate.getHours();
    var min = getDate.getMinutes();
    const getAl = hour - 12 >= 0 ? 'PM' : 'AM';
    if (getAl === 'PM') {
        hour = hour - 12 === 0 ? 12 : hour - 12;
    }
    hour = hour === 0 ? 12 : hour;
    day = `${day}`.length === 1 ? `0${day}` : day;
    month = month === 13 ? 12 : month;
    min = `${min}`.length === 1 ? `0${min}` : min;
    month = `${month}`.length === 1 ? `0${month}` : month;
    body[0].scannedDate = `${day}/${month}/${year}`;
    body[0].scannedTime = `${hour}:${min} ${getAl}`;
    body[0].synced = true;
    body[0].dateTime = `${body[0].scannedDate} ${body[0].scannedTime}`
    const {
        employeeId,
        siteId,
    } = body[0];
    try {
        if (employeeId === undefined || siteId === undefined) {
            const errorData = { message: 'All fields are mandatory' };
            return res.json({ errorData })
        }
        const newScan = new Scan(body[0]);
        const result = await newScan.save();
        res.json(result);

    } catch (error) {
        next(error);
    }

})

/*
 * Get All Scan
 */

.put(async(req, res, next) => {
    const { siteId, fromDate, lastDate } = req.body || {};
    const getData = getDateFormate({ fromDate, lastDate })
    const { fData, lData } = getData || {};
    try {
        const result = await Scan.find({ siteId: { $in: siteId }, scannedDate: { $gte: fData, $lte: lData } }).exec()
        res.json(result)
    } catch (error) {
        next(error)
    }
})

router.route('/allScans')

.get(async(req, res, next) => {
    console.log("hiihihihiihihihihihih");
    console.log('hey');
    try {
        console.log('hello');
        const result = await Scan.find({}).exec();
        console.log(result);
        res.json(result);
    } catch (error) {
        next(error)
    }
})



router.route('/:id')

/*
 * Get Particular Location find
 */

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { scanId: id };
    try {
        const result = await Scan.find(query).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})





router.route('/site/:id/:employeeId')

/*
 * Get Particular Location find
 */

.get(async(req, res, next) => {
    const { id, employeeId } = req.params;
    const query = { siteId: id, employeeId };
    try {
        const result = await Scan.find(query).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})



/*
 * Get Particular Location find And Update
 */

.put(async(req, res, next) => {
    const { id } = req.params;
    const query = { scanId: id };
    try {
        const result = await Scan.findOneAndUpdate(query, req.body, { new: true }).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})


router.route('/site/:id')

/*
 * Get Particular Location find
 */

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { siteId: id };
    try {
        const result = await Scan.find(query).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})

router.route('/pages/:start/:end')
    /*
     * Get Particular Provider find
     */
    .get(async(req, res, next) => {
        const { start, end } = req.params;
        try {
            const result = await Scan.find({}).skip(+start).limit(+end)
            res.json(result)
        } catch (error) {
            next(error)
        }
    })

router.route('/bulk')

/*
 * Register Many New Scan
 */

.post(async(req, res, next) => {
    const { body } = req;
    console.log(body)
    try {
        await Scan.insertMany(body);
        res.json({ message: 'created sucessfully' });

    } catch (error) {
        next(error);
    }

})




module.exports = router;