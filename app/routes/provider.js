'use strict';
const express = require('express');
const router = express.Router();
const { User, Provider } = require('../models');
const path = require('path');
const uuid = require('uuid');
const { PasswordServ } = require('../lib');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: './uploads/provider-log',
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid()}-${Date.now()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage });

router.route('/register')
    /*
     * Register Owner
     */

.post(upload.single('providerLogo'), async(req, res, next) => {
    const date = new Date().toISOString().split('T')[0];
    const { body } = req;
    Object.assign(body, {
            role: 'owner',
            providerAddress: 'Veniteck Solutions, Bangalore',
            noOfEmployees: 0,
            noOfGuards: 0,
            licenseStartDate: date,
            licenseTrailEndDate: date,
            licenseEndDate: date,
        })
        // body.role = 'Admin';
        // if (req.file) {
        //   body.profilePic = `/provider-log/${req.file.filename}`;
        // }
    const {
        email,
        providerName,
        role
    } = body;
    const query = {
        email
    };
    try {
        if (email === undefined || providerName === undefined) {
            const errorData = { message: 'All fields are mandatory' };
            return res.json({ errorData })
        }
        const user = await User.findOne(query).exec();

        if (user) {
            res.json({ message: 'User Already Exist' });
            const error = new Error('User Already Exist.. Please Login..!');
            error.status = 409;
            throw error;
        }

        const newUser = new User({
            email,
            role
        });
        const companyId = uuid();
        const result = await newUser.save();
        const userId = result.id;
        const password = await PasswordServ.hash(body.password);
        const profileData = {
            userId,
            password,
            companyId
        };

        Object.assign(body, profileData)
        const profile = new Provider(body);
        await profile.save();
        res.json({ message: 'created sucessfully' });

    } catch (error) {
        next(error);
    }

})

router.route('/')

/*
 * Register New Provider
 */

.post(upload.single('providerLogo'), async(req, res, next) => {
    const { body } = req;
    // body.role = 'Admin';
    if (req.file) {
        body.profilePic = `/provider-log/${req.file.filename}`;
    }
    const {
        email,
        providerName,
        role
    } = body;
    const query = {
        email
    };
    try {
        if (email === undefined || providerName === undefined) {
            const errorData = { message: 'All fields are mandatory' };
            return res.json({ errorData })
        }
        const user = await User.findOne(query).exec();

        if (user) {
            const error = new Error('User Already Exist.. Please Login..!');
            error.status = 409;
            throw error;
        }

        const newUser = new User({
            email,
            role
        });
        const companyId = uuid();
        const result = await newUser.save();
        const userId = result.id;
        const password = await PasswordServ.hash(body.password);
        const profileData = {
            userId,
            password,
            companyId
        };

        Object.assign(body, profileData)
        const profile = new Provider(body);
        await profile.save();
        res.json({ message: 'created sucessfully' });

    } catch (error) {
        next(error);
    }

})

/*
 * Get All Provider
 */

.get(async(req, res, next) => {
    try {
        const result = await Provider.find({}).exec()
        res.json(result)
    } catch (error) {
        next(error)
    }
})


router.route('/:id')

/*
 * Get Particular Provider find
 */

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { providerId: id };
    try {
        const result = await Provider.findOne(query).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})

/*
 * Get Particular Provider find And Update
 */

.put(upload.single('providerLogo'), async(req, res, next) => {
    const { id } = req.params;
    const query = { providerId: id };
    const { body } = req;
    if (req.file) {
        body.profilePic = `/provider-log/${req.file.filename}`;
    }
    try {
        const result = await Provider.findOneAndUpdate(query, body, { new: true }).exec()
        if (!result) {
            res.json({ message: 'No Record Found this ID' })
        }
        res.json(result)
    } catch (error) {
        next(error)
    }
})


router.route('/pages/:start/:end')
    /*
     * Get Particular Provider find
     */
    .get(async(req, res, next) => {
        const { start, end } = req.params;
        try {
            const countResult = await Provider.find({}).count()
            const result = await Provider.find({}).skip(+start).limit(+end)
            res.json({ result, count: countResult })
        } catch (error) {
            next(error)
        }
    })

module.exports = router;