'use strict';

const express = require('express');
const router = express.Router();
const { User, Employee } = require('../models');
const {
    SendPasswordResetMail
} = require('../helpers/mail');
const { PasswordServ } = require('../lib');


router.route('/companies/forgot')


    /**
     * Login
     * 
     */

    .post(async (req, res, next) => {

        const {
            companyEmail
        } = req.body;

        try {
            const user = await User.findOne({ email: companyEmail }).exec();

            if (!user) {
                const error = new Error('User Not Exist');
                error.status = 400;
                return next(error);
                // console.log('line 33....')
                // const message = 'User Not Exist';
                // const finalData = { success: false, payload: message }
                // console.log(finalData)
                // res.json({ finalData });
                //error.status = 400;
            } else {
                const otp = Math.floor(Math.random() * 10000)
                const profile = await Employee.findOne({ userId: user.id }).exec();
                await Object.assign(profile, { otp }).save();
                SendPasswordResetMail.send(await otp, companyEmail, profile.employeeName)
                    .then(data => { })
                    .catch(error => console.error(error));
                const message = 'OTP has been sent to your Email. Please use the OTP to reset your password'
                const finalData = { success: true, payload: message }
                res.json({ finalData });
            }
            // const otp = Math.floor(Math.random() * 10000)
            // const profile = await Employee.findOne({ userId: user.id }).exec();
            // await Object.assign(profile, { otp }).save();
            // SendPasswordResetMail.send(await otp, companyEmail, profile.employeeName)
            //     .then(data => { })
            //     .catch(error => console.error(error));
            // const message = 'OTP has been sent to your Email. Please use the OTP to reset your password'
            // const finalData = { success: true, payload: message }
            // res.json({ finalData });


        } catch (error) {
            next(error);
        }

    })

router.route('/companies/forgot/password')


    .put(async (req, res, next) => {

        const {
            companyEmail,
            companyPassword,
            verificationCode
        } = req.body;


        try {
            const user = await User.findOne({ email: companyEmail }).exec();

            if (!user) {
                const error = new Error('User Not Exist');
                error.status = 400;
                const message = 'User Not Exist'
                const errorData = { success: false, payload: message }
                res.json(errorData);
                return next(error);
            }

            const profile = await Employee.findOne({ userId: user.id, otp: verificationCode }).exec();
            if (!profile) {
                const error = new Error('Incorrect Password');
                error.status = 400;
                // const message = 'Incorrect OTP'
                // const finalData = { success: false, payload: message }
                // res.json(finalData);
                return next(error);
            }

            const hashPassword = await PasswordServ.hash(companyPassword);
            await Object.assign(profile, { password: hashPassword }).save();
            const message = 'Password Updated Successfully'
            const finalData = { success: true, payload: message }
            res.json({
                finalData
            });


        } catch (error) {
            next(error);
        }

    })

module.exports = router;