'use strict';
const express = require('express');
const router = express.Router();
const { Location } = require('../models');


router.route('/')

  /*
   * Register New Location
   */

  .post(async (req, res, next) => {
    const { body } = req;
    const {
      locationName,
      siteId,
    } = body;
    const query = {
      locationName,
      siteId
    };
    try {
      if (siteId === undefined || locationName === undefined) {
        const errorData = { message: 'All fields are mandatory' };
        return res.json({ errorData })
      }
      const location = await Location.findOne(query).exec();

      if (location) {
        const error = new Error('Location Already Exist...');
        error.status = 409;
        throw error;
      }

      const newLocation = new Location(body);

      await newLocation.save();
      res.json({ message: 'created sucessfully' });

    } catch (error) {
      next(error);
    }

  })

  /*
   * Get All Location
   */

  .get(async (req, res, next) => {
    try {
      const result = await Location.find({}).exec()
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/:id')

  /*
   * Get Particular Location find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { locationId: id };
    try {
      const result = await Location.findOne(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })

  /*
   * Get Particular Location find And Update
   */

  .put(async (req, res, next) => {
    const { id } = req.params;
    const query = { locationId: id };
    try {
      const result = await Location.findOneAndUpdate(query, req.body, { new: true }).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/site/:id')

  /*
   * Get Particular Location find
   */

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { siteId: id };
    try {
      const result = await Location.find(query).exec()
      if (!result) {
        res.json({ message: 'No Record Found this ID' })
      }
      res.json(result)
    } catch (error) {
      next(error)
    }
  })


router.route('/pages/:start/:end')
  /*
   * Get Particular Provider find
   */
  .get(async (req, res, next) => {
    const { start, end } = req.params;
    try {
      const result = await Location.find({}).skip(+start).limit(+end)
      res.json(result)
    } catch (error) {
      next(error)
    }
  })
module.exports = router;
