'use strict';
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

async function send(to, mailOptions) {
    const defaultOptions = {
        from: 'no_reply@araksha.com',
        subject: 'Email From Araksha - Gaurd Patroling System'
    };

    if (!to) {
        throw new Error('To Address Should Not Be Empty');
    }

    const message = Object.assign({}, defaultOptions, mailOptions, { to });
    return sgMail.send(message);
}


module.exports = {
    send
};