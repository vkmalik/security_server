'use strict';
const PasswordServ = require('./password');
const TokenServ = require('./token');
const SendMail = require('./send-mail');



module.exports = {
    PasswordServ,
    TokenServ,
    SendMail
}