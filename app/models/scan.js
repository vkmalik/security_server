'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const ScanSchema = new Schema({
    scanId: { type: String, default: uuid },
    siteName: { type: String, required: getRequiredFiledMessage('Site Name'), trim: true },
    locationName: { type: String, required: getRequiredFiledMessage('Location Name'), trim: true },
    longitude: { type: String, required: getRequiredFiledMessage('Longitude'), trim: true },
    latitude: { type: String, required: getRequiredFiledMessage('Latitude'), trim: true },
    companyId: { type: String, trim: true },
    siteId: { type: String, required: getRequiredFiledMessage('Site Id'), trim: true },
    providerId: { type: String, required: getRequiredFiledMessage('Provider Id'), trim: true },
    locationId: { type: String, required: getRequiredFiledMessage('Location Id'), trim: true },
    employeeId: { type: String, required: getRequiredFiledMessage('Employee Id'), trim: true },
    employeeName: { type: String, required: getRequiredFiledMessage('Employee Name'), trim: true },
    scannedType: {type:String, required: getRequiredFiledMessage('Scan Type'), trim: true},
    locDifference:  { type: String, trim: true },
    differnceMeters: {type:String, default:''},
    synced: {type:String, default: 'true'},
    beconeId: { type: String, default: '' }, 
    dateTime: { type: String, default: '' }, 
    scannedDate: { type: String, default: '' }, 
    scannedTime: { type: String, default: '' }
}, options);


const Scan = mongoose.model('Scan', ScanSchema);
module.exports = Scan;
