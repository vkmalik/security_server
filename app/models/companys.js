'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const CompanySchema = new Schema({
    companyId: { type: String, default: uuid },
    providerId: { type: String, required: getRequiredFiledMessage('Provider ID') },
    userId: { type: String, required: getRequiredFiledMessage('User ID') },
    companyName: { type: String, required: getRequiredFiledMessage('Company Name'), uniq: true },
    password: { type: String, required: getRequiredFiledMessage('Password') },
    companyAddress: { type: String, default: '' },
    companyIsoNumber: { type: String, default: '' },
    companyIsoPic: { type: String, default: '' },
    CompanyGstNumber: { type: String, default: '' },
    companyAccountNumber: { type: String, default: '' },
    CompanyIfscCode: { type: String, default: '' },
    CompanyBankName: { type: String, default: '' },
    CompanyBankBranch: { type: String, default: '' },
    CompanyBankAddress: { type: String, default: '' },
    licenseStartDate: { type: String, default: '' },
    licenseEndDate: { type: String, default: '' },
    licenseTrailEndDate: { type: String, default: '' },
    logo: { type: String, default: '' },
    emergencyContactPersonName: { type: String, default: '' },
    emergencyContactPersonNumber: { type: String, default: '' },
    isEmailVerified: { type: Boolean, default: true },
    profilePic: { type: String, default: '' },
    mobile: { type: String, default: '' },
    phone: { type: String, default: '' },
    city: { type: String, default: '' },
    country: { type: String, default: '' },
    noOfSites: { type: String, default: '' },
    noOfEmployees: { type: String, default: '0' },
    noOfGuards: { type: String, default: '' },
    status: { type: String, default: true },
    createdBy: { type: String, default: '' },
    otp: String
}, options);

const Company = mongoose.model('Company', CompanySchema);
module.exports = Company;