'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const SiteSchema = new Schema({
    siteId: { type: String, default: uuid },
    siteName: { type: String, required: getRequiredFiledMessage('Site Name'), trim: true, unique: true },
    siteNoOfQrcodes: { type: String, required: getRequiredFiledMessage('No Of Qrcodes'), trim: true },
    scanIntervals: { type: String, default: '', trim: true },
    Address: { type: String, required: getRequiredFiledMessage('Longitude'), trim: true },
    companyId: { type: String, required: getRequiredFiledMessage('Company Id'), trim: true },
    providerId: { type: String, required: getRequiredFiledMessage('Provider Id'), trim: true },
    autoSync: { type: String, default: 'No', trim: true },
    qrcode: { type: Boolean, default: false, trim: true },
    swap: { type: Boolean, default: false, trim: true },
    manual: { type: Boolean, default: false, trim: true },
    createdBy: { type: String, trim: true },
}, options);


const Site = mongoose.model('Site', SiteSchema);
module.exports = Site;
