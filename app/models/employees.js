'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const EmployeeSchema = new Schema({
    employeeId: { type: String, default: uuid },
    providerId: { type: String, required: getRequiredFiledMessage('Provider ID') },
    // companyId: { type: String, required: getRequiredFiledMessage('Comapny ID') },
    // siteId: { type: String, required: getRequiredFiledMessage('site ID') },
    userId: { type: String, required: getRequiredFiledMessage('User ID') },
    employeeName: { type: String, required: getRequiredFiledMessage('Employee Name') },
    password: { type: String, required: getRequiredFiledMessage('Password') },
    companies: { type: Array, default: [], trim: true },
    age: { type: String, default: '', trim: true },
    qualification: { type: String, default: '', trim: true },
    DOB: { type: String, default: '', trim: true },
    permanentAddress: { type: String, default: '', trim: true },
    presentAddress: { type: String, default: '', trim: true },
    joineDate: { type: String, default: '', trim: true },
    resineDate: { type: String, default: '', trim: true },
    maritalStatus: { type: String, default: '', trim: true },
    gender: { type: String, default: '', trim: true },
    governmentIdNumber: { type: String, default: '', trim: true },
    governmentIdCardFrontImg: { type: String, default: '', trim: true },
    governmentIdCardBackImg: { type: String, default: '', trim: true },
    workingExperince: { type: String, default: '', trim: true },
    lastCompanyname: { type: String, default: '', trim: true },
    lastCompanyAddress: { type: String, default: '', trim: true },
    previousSalary: { type: String, default: '', trim: true },
    presentSalary: { type: String, default: '', trim: true },
    employeeAccountNumber: { type: String, default: '' },
    employeeIfscCode: { type: String, default: '' },
    employeeBankName: { type: String, default: '' },
    employeeBankBranch: { type: String, default: '' },
    employeeBankAddress: { type: String, default: '' },
    emergencyContactPersonName: { type: String, default: '' },
    emergencyContactPersonNumber: { type: String, default: '' },
    isEmailVerified: { type: Boolean, default: true },
    profilePic: { type: String, default: '' },
    typeOfIdProof: { type: String, default: '' },
    mobile: { type: String, default: '' },
    city: { type: String, default: '' },
    role: { type: String, default: '' },
    country: { type: String, default: '' },
    isLogin: { type: Boolean, default: false },
    status: { type: Boolean, default: true },
    createdBy: { type: String, default: '' },
    otp: String
}, options);

const Employee = mongoose.model('Employee', EmployeeSchema);
module.exports = Employee;