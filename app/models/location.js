'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const LocationSchema = new Schema({
    locationId: { type: String, default: uuid },
    locationName: { type: String, required: getRequiredFiledMessage('Location Name'), trim: true, unique: true},
    longitude: { type: String, trim: true },
    latitude: { type: String, trim: true },
    siteId: { type: String, required: getRequiredFiledMessage('Site Id'), trim: true},
    providerId: { type: String, required: getRequiredFiledMessage('Provider Id'), trim: true },
    locDifference:  { type: String, required: getRequiredFiledMessage('Location Difference'), trim: true },
    beconeId: { type: String, default: '' }, 
    createdBy: { type: String, required: getRequiredFiledMessage('Created Id'), trim: true },
}, options);


const Location = mongoose.model('Location', LocationSchema);
module.exports = Location;
