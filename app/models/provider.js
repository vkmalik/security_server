'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
  timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const ProviderSchema = new Schema({
  providerId: { type: String, default: uuid },
  userId: { type: String, required: getRequiredFiledMessage('User ID') },
  providerName: { type: String, required: getRequiredFiledMessage('Provider Name'), unique: true },
  password: { type: String, required: getRequiredFiledMessage('Password') },
  providerAddress: { type: String, required: getRequiredFiledMessage('Address') },
  isEmailVerified: { type: Boolean, default: true },
  profilePic: {type: String, default: ''},
  phone: {type: String, default: ''},
  mobile: {type: String, default: ''},
  city: {type: String, default: ''},
  country: {type: String, default: ''},
  // noOfCompanys: {type: String, required: getRequiredFiledMessage('no Of Companys')},
  noOfEmployees: {type: String, required: getRequiredFiledMessage('no Of Employees')},
  noOfGuards: {type: String, required: getRequiredFiledMessage('no Of Guards')},
  licenseStartDate: {type: String, required: getRequiredFiledMessage('license Start Date')},
  licenseTrailEndDate: {type: String, required: getRequiredFiledMessage('license Trail End Date')},
  licenseEndDate: {type: String, required: getRequiredFiledMessage('license End Date')},
  createdBy:{ type: String, default: '' },
  status: { type:String, default: true },
  otp: String,
  role: { type: String, default: '' },
  companyId: { type: String, default: '' }
}, options);

const Provider = mongoose.model('Provider', ProviderSchema);
module.exports = Provider;