'use strict';

const db = require('./db');
const User = require('./user');
const Company = require('./companys');
const Provider = require('./provider');
const Employee = require('./employees');
const Location = require('./location');
const Scan = require('./scan');
const Site = require('./sites');


module.exports = {
    db,
    Company,
    User,
    Provider,
    Employee,
    Location,
    Scan,
    Site
}